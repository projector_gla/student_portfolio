var Router= require('koa-router');
var bodyParser = require('koa-body')();

module.exports = function(app){

    var router = new Router();
  
 var welcomeCtrl = require('./../controllers/welcomeCtrl');
 var searchCtrl  = require('./../controllers/searchCtrl');
 var teacherctrl = require('./../controllers/teacherctrl');
 var student_homectrl = require('./../controllers/student_homectrl');
 
 router.get('/home', welcomeCtrl.showHomePage);
 router.post('/home', welcomeCtrl.searchskill);
 
 router.get('/student_home', student_homectrl.showstudent_homepage);
 router.post('/student_home', student_homectrl.studentsearchskill);
 
 router.get('/studentlogout', student_homectrl.logout);
 router.get('/teacherlogout', teacherctrl.logout);
 
 router.get('/search', searchCtrl.showSearchPage);  
 router.post('/search', searchCtrl.multiplesearchskill);
 
 router.get('/teacherhome', teacherctrl.showteacherPage);
 router.post('/teacherhome', teacherctrl.teachersearchskill);
 

    return router.middleware();
}
