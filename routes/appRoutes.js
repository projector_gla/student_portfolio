var Router= require('koa-router');
var bodyParser = require('koa-body')();

module.exports = function(app){

    var router = new Router();


  
  var resumeCtrl = require('./../controllers/resumeCtrl');
  var signinCtrl  = require('./../controllers/SigninCtrl');
  var signupCtrl = require('./../controllers/signupCtrl');
  var updateCtrl = require('./../controllers/updateCtrl');

  
    router.get('/resume/:id', resumeCtrl.showresumePage);
    router.post('/signin', signinCtrl.verifyLoginPage);

    router.post('/verify', signinCtrl.verify);
    router.post('/verifypassword', signinCtrl.verifypassword);
    
    router.get('/signup', signupCtrl.signup);
    router.post('/signup', signupCtrl.signupPage);
    
    router.post('/update', updateCtrl.resumePage);
    router.get('/update/:id', updateCtrl.resume);
   


    return router.middleware();
}
