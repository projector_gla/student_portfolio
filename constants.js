module.exports = {
    MYSQL_ERROR_CODES: {
        DUPLICATE_ENTRY: "ER_DUP_ENTRY"
    },
    redisDataKeys: {
      SKILL_LIST : "skill_array"
    } 
};
