-- MySQL dump 10.13  Distrib 8.0.11, for Win64 (x86_64)
--
-- Host: localhost    Database: student_portfolio
-- ------------------------------------------------------
-- Server version	8.0.11

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8mb4 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `course`
--

DROP TABLE IF EXISTS `course`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `course` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `course_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `qualification_id` int(10) unsigned NOT NULL,
  `creation_timestamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `course_name` (`course_name`),
  KEY `qualification_id` (`qualification_id`),
  CONSTRAINT `course_ibfk_1` FOREIGN KEY (`qualification_id`) REFERENCES `qualification` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `course`
--

LOCK TABLES `course` WRITE;
/*!40000 ALTER TABLE `course` DISABLE KEYS */;
INSERT INTO `course` VALUES (1,'B.Tech',3,'2018-07-03 00:17:01'),(2,'BBA',3,'2018-07-03 00:17:01'),(3,'BCA',3,'2018-07-03 00:17:01'),(4,'B.Com',3,'2018-07-03 00:17:01'),(5,'B.Pharma',3,'2018-07-03 00:17:01'),(6,'B.Sc',3,'2018-07-03 00:17:02'),(7,'B.Ed',3,'2018-07-03 00:17:02'),(8,'Diploma',3,'2018-07-03 00:17:02'),(9,'BA',3,'2018-07-03 00:17:02'),(10,'D.Pharma',3,'2018-07-03 00:17:02'),(11,'M.Tech',4,'2018-07-03 00:17:02'),(12,'MBA',4,'2018-07-03 00:17:02'),(13,'MCA',4,'2018-07-03 00:17:02'),(14,'MSC',4,'2018-07-03 00:17:02'),(15,'M.Pharma',4,'2018-07-03 00:17:02'),(16,'MA',4,'2018-07-03 00:17:02'),(17,'P.H.D',5,'2018-07-03 00:17:02'),(18,'Applied Science and Humanities',3,'2018-07-03 00:17:02');
/*!40000 ALTER TABLE `course` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `course_department`
--

DROP TABLE IF EXISTS `course_department`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `course_department` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `course_id` int(10) unsigned NOT NULL,
  `department_id` int(10) unsigned NOT NULL,
  `creation_timestamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `course_id` (`course_id`),
  KEY `department_id` (`department_id`),
  CONSTRAINT `course_department_ibfk_1` FOREIGN KEY (`course_id`) REFERENCES `course` (`id`),
  CONSTRAINT `course_department_ibfk_2` FOREIGN KEY (`department_id`) REFERENCES `department` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `course_department`
--

LOCK TABLES `course_department` WRITE;
/*!40000 ALTER TABLE `course_department` DISABLE KEYS */;
INSERT INTO `course_department` VALUES (1,1,1,'2018-07-03 00:17:03'),(2,1,2,'2018-07-03 00:17:03'),(3,1,3,'2018-07-03 00:17:03'),(4,1,4,'2018-07-03 00:17:03'),(5,1,5,'2018-07-03 00:17:03'),(6,1,6,'2018-07-03 00:17:03'),(7,1,7,'2018-07-03 00:17:03'),(8,1,8,'2018-07-03 00:17:03'),(9,1,9,'2018-07-03 00:17:03'),(10,1,10,'2018-07-03 00:17:03'),(11,11,1,'2018-07-03 00:17:03'),(12,11,2,'2018-07-03 00:17:03'),(13,11,3,'2018-07-03 00:17:03'),(14,11,4,'2018-07-03 00:17:03'),(15,11,5,'2018-07-03 00:17:03'),(16,11,6,'2018-07-03 00:17:03'),(17,11,7,'2018-07-03 00:17:03'),(18,11,8,'2018-07-03 00:17:03'),(19,18,11,'2018-07-03 00:17:03'),(20,18,12,'2018-07-03 00:17:03'),(21,18,13,'2018-07-03 00:17:03'),(22,18,14,'2018-07-03 00:17:03'),(23,18,15,'2018-07-03 00:17:03'),(24,8,1,'2018-07-03 00:17:03'),(25,8,2,'2018-07-03 00:17:03'),(26,8,3,'2018-07-03 00:17:03'),(27,8,4,'2018-07-03 00:17:03'),(28,8,6,'2018-07-03 00:17:03'),(29,8,16,'2018-07-03 00:17:03'),(30,8,17,'2018-07-03 00:17:03'),(31,12,20,'2018-07-03 00:17:03'),(32,12,21,'2018-07-03 00:17:03'),(33,12,22,'2018-07-03 00:17:03'),(34,14,18,'2018-07-03 00:17:03'),(35,14,24,'2018-07-03 00:17:03'),(36,14,13,'2018-07-03 00:17:03'),(37,14,31,'2018-07-03 00:17:03'),(38,15,25,'2018-07-03 00:17:03'),(39,15,26,'2018-07-03 00:17:03'),(40,15,27,'2018-07-03 00:17:03');
/*!40000 ALTER TABLE `course_department` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `department`
--

DROP TABLE IF EXISTS `department`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `department` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `department_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `creation_timestamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `department_name` (`department_name`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `department`
--

LOCK TABLES `department` WRITE;
/*!40000 ALTER TABLE `department` DISABLE KEYS */;
INSERT INTO `department` VALUES (1,'Civil Engineering','2018-07-03 00:17:02'),(2,'Electrical Engineering','2018-07-03 00:17:02'),(3,'Mechanical Engineering','2018-07-03 00:17:02'),(4,'Electronics and Communication Engineering','2018-07-03 00:17:02'),(5,'Electrical and Electronics Engineering','2018-07-03 00:17:02'),(6,'Computer Science and Engineering','2018-07-03 00:17:02'),(7,'Cloud Computing and Virtualization','2018-07-03 00:17:02'),(8,'Data Analytics','2018-07-03 00:17:02'),(9,'Cyber Security and Forensics','2018-07-03 00:17:02'),(10,'Engineering in Information s_name','2018-07-03 00:17:02'),(11,'Mathematics','2018-07-03 00:17:02'),(12,'Physics','2018-07-03 00:17:02'),(13,'Chemistry','2018-07-03 00:17:02'),(14,'English','2018-07-03 00:17:02'),(15,'Social Science','2018-07-03 00:17:02'),(16,'Chemical Engg.','2018-07-03 00:17:02'),(17,'Pharmacy','2018-07-03 00:17:02'),(18,'Biotech','2018-07-03 00:17:02'),(19,'Global Accounting in association with CIMA','2018-07-03 00:17:02'),(20,'Logistic and Supply Chain Management','2018-07-03 00:17:02'),(21,'Family Business','2018-07-03 00:17:02'),(22,'Financial Markets and Banking','2018-07-03 00:17:02'),(23,'Economics','2018-07-03 00:17:03'),(24,'Microbiology and Immunology','2018-07-03 00:17:03'),(25,'Pharmology','2018-07-03 00:17:03'),(26,'Pharmaceutics','2018-07-03 00:17:03'),(27,'Pharmaceutical Chemistry','2018-07-03 00:17:03'),(28,'Humanities and Science','2018-07-03 00:17:03'),(29,'LLB','2018-07-03 00:17:03'),(30,'Hons.','2018-07-03 00:17:03'),(31,'Applied Physics','2018-07-03 00:17:03');
/*!40000 ALTER TABLE `department` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `qualification`
--

DROP TABLE IF EXISTS `qualification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `qualification` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `qualification` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `creation_timestamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `qualification` (`qualification`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `qualification`
--

LOCK TABLES `qualification` WRITE;
/*!40000 ALTER TABLE `qualification` DISABLE KEYS */;
INSERT INTO `qualification` VALUES (1,'10th','2018-07-03 00:17:01'),(2,'12th','2018-07-03 00:17:01'),(3,'UG','2018-07-03 00:17:01'),(4,'PG','2018-07-03 00:17:01'),(5,'Doctoral','2018-07-03 00:17:01');
/*!40000 ALTER TABLE `qualification` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `role` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `role` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'NOT ASSIGNED',
  `creation_timestamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `role` (`role`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` VALUES (1,'Admin','2018-07-03 00:17:01'),(2,'Moderator','2018-07-03 00:17:01'),(3,'Student','2018-07-03 00:17:01');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `skill`
--

DROP TABLE IF EXISTS `skill`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `skill` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `skill_type` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `skill_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `creation_timestamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `skill_name` (`skill_name`)
) ENGINE=InnoDB AUTO_INCREMENT=145 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `skill`
--

LOCK TABLES `skill` WRITE;
/*!40000 ALTER TABLE `skill` DISABLE KEYS */;
INSERT INTO `skill` VALUES (1,'Programming Language','C','2018-07-03 00:17:04'),(2,'Programming Language','C++','2018-07-03 00:17:04'),(3,'Programming Language','JAVA','2018-07-03 00:17:04'),(4,'Programming Language','Python','2018-07-03 00:17:04'),(5,'Programming Language','C#','2018-07-03 00:17:04'),(6,'Programming Language','PHP','2018-07-03 00:17:04'),(7,'Programming Language','R','2018-07-03 00:17:04'),(8,'Programming Language','BASIS','2018-07-03 00:17:04'),(9,'Programming Language','HTML','2018-07-03 00:17:04'),(10,'Programming Language','CSS','2018-07-03 00:17:04'),(11,'Programming Language','SQL','2018-07-03 00:17:04'),(12,'Programming Language','PL/SQL','2018-07-03 00:17:04'),(13,'Programming Language','XML','2018-07-03 00:17:04'),(14,'Programming Language','Ruby','2018-07-03 00:17:04'),(15,'Programming Language','Perl','2018-07-03 00:17:04'),(16,'Programming Language','UML','2018-07-03 00:17:04'),(17,'Programming Language','Pascal','2018-07-03 00:17:04'),(18,'Programming Language','.Net','2018-07-03 00:17:04'),(19,'Programming Language','Kotlin','2018-07-03 00:17:04'),(20,'Programming Language','JavaScript','2018-07-03 00:17:04'),(21,'Hardware and Software','ARDUINO','2018-07-03 00:17:04'),(22,'Hardware and Software','RASPBERRYPI','2018-07-03 00:17:04'),(23,'Hardware and Software','AUTOCAD','2018-07-03 00:17:04'),(24,'Hardware and Software','CAD','2018-07-03 00:17:04'),(25,'Hardware and Software','ANSYS','2018-07-03 00:17:04'),(26,'Hardware and Software','R STUDIO','2018-07-03 00:17:04'),(27,'Hardware and Software','Adobe Photoshop','2018-07-03 00:17:05'),(28,'Hardware and Software','CIVIL 3D','2018-07-03 00:17:05'),(29,'Hardware and Software','JS Bin','2018-07-03 00:17:05'),(30,'Hardware and Software','MATHCAD','2018-07-03 00:17:05'),(31,'Hardware and Software','VISUAL STUDIO','2018-07-03 00:17:05'),(32,'Hardware and Software','Liquid XML Studio','2018-07-03 00:17:05'),(33,'Hardware and Software','ECLIPS','2018-07-03 00:17:05'),(34,'Technology','Computer Networking and Internet Protocols','2018-07-03 00:17:05'),(35,'Technology','Cyber Security','2018-07-03 00:17:05'),(36,'Technology','Database Management System','2018-07-03 00:17:05'),(37,'Technology','Software engineering','2018-07-03 00:17:05'),(38,'Technology','Website Design','2018-07-03 00:17:05'),(39,'Technology','Website Development','2018-07-03 00:17:05'),(40,'Technology','Deep Learning','2018-07-03 00:17:05'),(41,'Technology','Big Data','2018-07-03 00:17:05'),(42,'Technology','Computer Architecture','2018-07-03 00:17:05'),(43,'Technology','Cloud Computing','2018-07-03 00:17:05'),(44,'Technology','Data Analytics','2018-07-03 00:17:05'),(45,'Technology','Machine Learning','2018-07-03 00:17:05'),(46,'Technology','Artificial Intelligence','2018-07-03 00:17:05'),(47,'Technology','Data Mining','2018-07-03 00:17:05'),(48,'Technology','Data Science','2018-07-03 00:17:05'),(49,'Technology','Mobile Applications','2018-07-03 00:17:05'),(50,'Technology','Internet of Things','2018-07-03 00:17:05'),(51,'Technology','Microprocessor','2018-07-03 00:17:05'),(52,'Technology','Blockchain Architecture','2018-07-03 00:17:05'),(53,'Technology','Digital Image Processing','2018-07-03 00:17:05'),(54,'Management','Logistic Management','2018-07-03 00:17:05'),(55,'Management','Marketing','2018-07-03 00:17:05'),(56,'Management','Human Resource Development','2018-07-03 00:17:05'),(57,'Management','Finance','2018-07-03 00:17:05'),(58,'Management','Infromation System','2018-07-03 00:17:05'),(59,'Management','Consulting','2018-07-03 00:17:05'),(60,'Management','Entrepreneurship','2018-07-03 00:17:05'),(61,'Management','Supply Chain Management','2018-07-03 00:17:05'),(62,'Management','Leadership','2018-07-03 00:17:05'),(63,'Management','Economic Growth and Development','2018-07-03 00:17:05'),(64,'Management','Innovation','2018-07-03 00:17:05'),(65,'Management','Business Models','2018-07-03 00:17:05'),(66,'Management','Simulation of Business System','2018-07-03 00:17:05'),(67,'Management','Sustainability through Green Manufacturing System','2018-07-03 00:17:05'),(68,'Management','Total Quality Management','2018-07-03 00:17:05'),(69,'Management','Microeconomics','2018-07-03 00:17:05'),(70,'Management','Engineering Economics','2018-07-03 00:17:05'),(71,'Management','Project management for managers','2018-07-03 00:17:05'),(72,'Management','E-Business','2018-07-03 00:17:05'),(73,'Management','Capital Management','2018-07-03 00:17:05'),(74,'Management','Management of Inventory Systems','2018-07-03 00:17:05'),(75,'Bio Science','Community pharmacy','2018-07-03 00:17:05'),(76,'Bio Science','Hospital pharmacy','2018-07-03 00:17:05'),(77,'Bio Science','Clinical pharmacy','2018-07-03 00:17:05'),(78,'Bio Science','Industrial pharmacy','2018-07-03 00:17:05'),(79,'Bio Science','Regulatory pharmacy','2018-07-03 00:17:05'),(80,'Bio Science','Home care pharmacy','2018-07-03 00:17:05'),(81,'Bio Science','Managed care pharmacy','2018-07-03 00:17:05'),(82,'Bio Science','Research pharmacy','2018-07-03 00:17:05'),(83,'Bio Science','Oncology pharmacy','2018-07-03 00:17:05'),(84,'Bio Science','Ambulatory care pharmacy','2018-07-03 00:17:05'),(85,'Bio Science','Compounding pharmacy','2018-07-03 00:17:05'),(86,'Bio Science','Consultant pharmacy','2018-07-03 00:17:05'),(87,'Bio Science','Internet pharmacy','2018-07-03 00:17:05'),(88,'Bio Science','Veterinary pharmacy','2018-07-03 00:17:05'),(89,'Bio Science','Nuclear pharmacy','2018-07-03 00:17:06'),(90,'Bio Science','Military pharmacy','2018-07-03 00:17:06'),(91,'Bio Science','Specialty pharmacy','2018-07-03 00:17:06'),(92,'Bio Science','Geriatric pharmacy','2018-07-03 00:17:06'),(93,'Bio Science','Psychopharmacs_nameapy','2018-07-03 00:17:06'),(94,'Bio Science','Personal pharmacy','2018-07-03 00:17:06'),(95,'Bio Science','Nutritional support pharmacy','2018-07-03 00:17:06'),(96,'Bio Science','Hospice pharmacy','2018-07-03 00:17:06'),(97,'Bio Science','Pediatric pharmacy','2018-07-03 00:17:06'),(98,'Bio Science','Pharmacy benefit manager','2018-07-03 00:17:06'),(99,'Bio Science','Poison control pharmacy','2018-07-03 00:17:06'),(100,'Art and Humanities','Archeaology','2018-07-03 00:17:06'),(101,'Art and Humanities','History','2018-07-03 00:17:06'),(102,'Art and Humanities','Law','2018-07-03 00:17:06'),(103,'Art and Humanities','Politics','2018-07-03 00:17:06'),(104,'Art and Humanities','Literature','2018-07-03 00:17:06'),(105,'Art and Humanities','Religion','2018-07-03 00:17:06'),(106,'Art and Humanities','Philosophy','2018-07-03 00:17:06'),(107,'Art and Humanities','Linguistics','2018-07-03 00:17:06'),(108,'Art and Humanities','Intellectual Property','2018-07-03 00:17:06'),(109,'Art and Humanities','Culture studies','2018-07-03 00:17:06'),(110,'Art and Humanities','Soft skills','2018-07-03 00:17:06'),(111,'Art and Humanities','Educational Leadership','2018-07-03 00:17:06'),(112,'Art and Humanities','Development of Soclology','2018-07-03 00:17:06'),(113,'Art and Humanities','Visual perception','2018-07-03 00:17:06'),(114,'Art and Humanities','Tranformation and Lives','2018-07-03 00:17:06'),(115,'Game','Cricket','2018-07-03 00:17:06'),(116,'Game','Volleyball','2018-07-03 00:17:06'),(117,'Game','Basketball','2018-07-03 00:17:06'),(118,'Game','Chess','2018-07-03 00:17:06'),(119,'Game','Swimming','2018-07-03 00:17:06'),(120,'Game','Cycling','2018-07-03 00:17:06'),(121,'Game','Hockey','2018-07-03 00:17:06'),(122,'Game','Badminton','2018-07-03 00:17:06'),(123,'Game','Tennis','2018-07-03 00:17:06'),(124,'Game','Table Tennis','2018-07-03 00:17:06'),(125,'Game','Wrestling','2018-07-03 00:17:06'),(126,'Game','Football','2018-07-03 00:17:06'),(127,'Game','Boxing','2018-07-03 00:17:06'),(128,'Game','Golf','2018-07-03 00:17:06'),(129,'Game','Archery','2018-07-03 00:17:06'),(130,'Game','Athletics','2018-07-03 00:17:06'),(131,'Game','Gymnastics','2018-07-03 00:17:06'),(132,'Game','Handball','2018-07-03 00:17:06'),(133,'Game','Kabaddi','2018-07-03 00:17:06'),(134,'Game','Throwball','2018-07-03 00:17:06'),(135,'Game','Weightlifting','2018-07-03 00:17:06'),(136,'Game','Karate','2018-07-03 00:17:06'),(137,'Game','Kho-kho','2018-07-03 00:17:06'),(138,'Artist','Painting','2018-07-03 00:17:06'),(139,'Artist','Dancing','2018-07-03 00:17:06'),(140,'Artist','Photography','2018-07-03 00:17:06'),(141,'Artist','Singing','2018-07-03 00:17:06'),(142,'Artist','Acting','2018-07-03 00:17:06'),(143,'Artist','Writing','2018-07-03 00:17:06'),(144,'Artist','Crafting','2018-07-03 00:17:06');
/*!40000 ALTER TABLE `skill` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `university_id` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `photo` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email_id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` bigint(13) DEFAULT NULL,
  `active` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'YES',
  `creation_timestamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`,`university_id`),
  UNIQUE KEY `university_id` (`university_id`)
) ENGINE=InnoDB AUTO_INCREMENT=73 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'161500100','12345','Akshay kumar','static/uploads/upload_7b10a11686a54a53191931a8d69d8a5e','akshay.katiha_cs16@gla.ac.in',9876543210,'YES','2018-07-03 00:17:00'),(2,'161500101','23456','Vivek Kumar','static/uploads/upload_562d36572d2f163f26559513c3cbfb47','vivek.kumar_cs16@gla.ac.in',9753186420,'YES','2018-07-03 00:17:00'),(3,'161500102','34567','Akrati Chaturvedi','static/uploads/upload_7b10a11686a54a53191931a8d69d8a5e','akrati.chaturvedi_cs16@gla.ac.in',1234567890,'YES','2018-07-03 00:17:00'),(4,'161400103','45678','Raghav Agravel',NULL,'raghav.agravel_cs16@gla.ac.in',1236987450,'YES','2018-07-03 00:17:00'),(5,'151500054','56789','Raghav Sharma',NULL,'raghav_cs16@gla.ac.in',9874563210,'YES','2018-07-03 00:17:00'),(6,'151600084','67890','Ravi Vyas',NULL,'ravi_cs16@gla.ac.in',9512376408,'YES','2018-07-03 00:17:00'),(7,'171300059','11111','Devesh Sharma',NULL,'devesh.sharma_cs16@gla.ac.in',9112233456,'YES','2018-07-03 00:17:00'),(8,'141400011','22222','Harshit Kumar',NULL,'harshit.kumar_cs16@gla.ac.in',5589635490,'YES','2018-07-03 00:17:00'),(9,'141500020','33333','Harshita Verma',NULL,'harshita.verma_cs16@gla.ac.in',1234560258,'YES','2018-07-03 00:17:00'),(10,'171600054','44444','Hritik Tiwari',NULL,'hritik.tiwari_cs16@gla.ac.in',8549687561,'YES','2018-07-03 00:17:00'),(11,'161500060','55555','Jitendra Mittal',NULL,'jitendramittal@gmail.com',2584698753,'YES','2018-07-03 00:17:00'),(12,'141500089','85497','Kavita Singh',NULL,'kavitasingh@gmail.com',5589746512,'YES','2018-07-03 00:17:00'),(13,'151500012','99999','Mansi Bhatt',NULL,'mansi.bhatt_cs16@gla.ac.in',9856741230,'YES','2018-07-03 00:17:00'),(14,'121400036','22569','Kuldeep Singh',NULL,'kuldeepsingh_cs16@gla.ac.in',6879456123,'YES','2018-07-03 00:17:00'),(15,'161300111','00156','Kavyansh',NULL,'kavyansh_cs16@gla.ac.in',9632587410,'YES','2018-07-03 00:17:00'),(49,'12345','123','dfg','static\\static\\uploads\\upload_6e85c96c3c1edd3e82685deac5c16cf1','qwert@fghj',1234,'YES','2018-07-04 11:58:17'),(50,'12121','1111','ssss','static\\static\\uploads\\upload_391145b3f99a1d31f6e425d2255f73e4','asasa@qwq',12112121,'YES','2018-07-04 22:29:03'),(51,'1615qw','12345','piyush',NULL,NULL,NULL,'YES','2018-07-05 23:59:25'),(52,'3','111','RR','static\\static\\uploads\\upload_aa256324b789f6784f88e3c84971a161','SDFG@SDFG',45678,'YES','2018-07-07 00:28:04'),(53,'100','qw','ft','static\\static\\uploads\\upload_d23b703c888314f49b8d4d5aeab5d376','vi@qq',6545,'YES','2018-07-07 01:16:30'),(55,'6','11','rt','static\\static\\uploads\\upload_1baf9e577c29e744e4c42facc2e528c5','sdfs@wew',5678,'YES','2018-07-07 01:17:14'),(56,'11w','11','aed','static\\static\\uploads\\upload_238ddb7d7f1df1b25cfe810f91eef076','ad@wed',34,'YES','2018-07-10 00:28:32'),(57,'33','11','qwe','static\\static\\uploads\\upload_126890a4b1c8777860e2fba7ed6e2e53','wdw@ew',6543,'YES','2018-07-10 00:30:42'),(58,'121212','123','ssa','static\\static\\uploads\\upload_799ef2ff731493b4ed4137816c684832','dfvs@xwwd',7654,'YES','2018-07-10 03:37:27'),(59,'4','111','wew','static\\static\\uploads\\upload_bc28e0f7914c763174dcc39d090cd15e','wdwd@dw',43342,'YES','2018-07-10 03:41:56'),(60,'56','121','sfse','static//static//uploads//upload_bd4f16f94b1b5f22ee1859663a79d86b','ace@d',12341,'YES','2018-07-10 03:44:22'),(61,'66','111','cdc','static/static/uploads/upload_df8fca57c7b118343a84fcaf86696b08','ddfs@wd',76543,'YES','2018-07-10 03:52:10'),(62,'8','111','reyt','staticstaticuploadsupload_e9a6f8af7821716f709da4345a6ca4a7','tgfd@tgfghj',4567,'YES','2018-07-10 04:04:19'),(63,'127777','kiki','agsdfdty','static/uploads/upload_562d36572d2f163f26559513c3cbfb47','a@a',899999,'YES','2018-07-10 04:06:01'),(64,'adfea','3456','sda','static/uploads/upload_7b10a11686a54a53191931a8d69d8a5e','fxh@tgfd',476,'YES','2018-07-09 23:42:51'),(65,'161500376','98765','Parth','static/uploads/upload_5a609d5cbe0a4a849ec9b2631f8b4745','part@gmail.com',9797977,'YES','2018-07-10 01:11:05'),(66,'','',NULL,NULL,'',NULL,'YES','2018-07-13 22:06:08'),(67,'1212','123',NULL,NULL,'asdfgh@aszdfcg',NULL,'YES','2018-07-13 22:22:02'),(69,'undefined','undefined',NULL,NULL,'undefined',NULL,'YES','2018-07-14 13:33:04'),(71,'123456789','12345',NULL,NULL,'qwerty@qwert',NULL,'YES','2018-07-14 13:58:53'),(72,'qwerty','12345',NULL,NULL,'werty@qwert',NULL,'YES','2018-07-14 15:50:09');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_qualification`
--

DROP TABLE IF EXISTS `user_qualification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `user_qualification` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `qualification_id` int(10) unsigned NOT NULL,
  `course_id` int(10) unsigned DEFAULT NULL,
  `department_id` int(10) unsigned DEFAULT NULL,
  `report_card` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `passing_year` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `board` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `percentage` float(7,4) DEFAULT NULL,
  `verified` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'NO',
  `creation_timestamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `qualification_id` (`qualification_id`),
  KEY `course_id` (`course_id`),
  KEY `department_id` (`department_id`),
  CONSTRAINT `user_qualification_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  CONSTRAINT `user_qualification_ibfk_2` FOREIGN KEY (`qualification_id`) REFERENCES `qualification` (`id`),
  CONSTRAINT `user_qualification_ibfk_3` FOREIGN KEY (`course_id`) REFERENCES `course` (`id`),
  CONSTRAINT `user_qualification_ibfk_4` FOREIGN KEY (`department_id`) REFERENCES `department` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_qualification`
--

LOCK TABLES `user_qualification` WRITE;
/*!40000 ALTER TABLE `user_qualification` DISABLE KEYS */;
INSERT INTO `user_qualification` VALUES (1,1,1,NULL,NULL,'undefined','2014','C.B.S.E.',95.0000,'NO','2018-07-03 00:17:04'),(2,1,2,NULL,NULL,'undefined','2016','C.B.S.E.',80.8000,'NO','2018-07-03 00:17:04'),(3,1,3,1,1,'undefined','2020','undefined',92.0000,'NO','2018-07-03 00:17:04'),(4,2,1,NULL,NULL,NULL,'2014','C.B.S.E.',95.0000,'NO','2018-07-03 00:17:04'),(5,2,2,NULL,NULL,NULL,'2016','C.B.S.E.',80.8000,'NO','2018-07-03 00:17:04'),(6,2,3,1,6,NULL,'2020','Gla University,Mathura',92.0000,'NO','2018-07-03 00:17:04'),(7,3,1,NULL,NULL,NULL,'2014','C.B.S.E.',95.2000,'NO','2018-07-03 00:17:04'),(8,3,2,NULL,NULL,NULL,'2016','C.B.S.E.',84.8000,'NO','2018-07-03 00:17:04'),(9,3,3,1,6,NULL,'2020','Gla University,Mathura',95.5800,'NO','2018-07-03 00:17:04'),(10,4,1,NULL,NULL,NULL,'2015','C.B.S.E.',98.2000,'NO','2018-07-03 00:17:04'),(11,4,2,NULL,NULL,NULL,'2017','C.B.S.E.',88.8000,'NO','2018-07-03 00:17:04'),(12,4,3,1,7,NULL,'2020','Gla University,Mathura',96.7700,'NO','2018-07-03 00:17:04'),(13,5,1,NULL,NULL,NULL,'2014','C.B.S.E.',97.2000,'NO','2018-07-03 00:17:04'),(14,5,2,NULL,NULL,NULL,'2016','C.B.S.E.',94.8000,'NO','2018-07-03 00:17:04'),(15,5,3,1,8,NULL,'2020','Gla University,Mathura',98.3600,'NO','2018-07-03 00:17:04'),(16,6,1,NULL,NULL,NULL,'2012','C.B.S.E.',98.2000,'NO','2018-07-03 00:17:04'),(17,6,2,NULL,NULL,NULL,'2014','C.B.S.E.',99.8000,'NO','2018-07-03 00:17:04'),(18,6,3,1,8,NULL,'2020','Gla University,Mathura',90.3300,'NO','2018-07-03 00:17:04'),(19,6,4,11,7,NULL,'2022','Gla University,Mathura',85.9900,'NO','2018-07-03 00:17:04');
/*!40000 ALTER TABLE `user_qualification` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_role`
--

DROP TABLE IF EXISTS `user_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `user_role` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  `creation_timestamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `role_id` (`role_id`),
  CONSTRAINT `user_role_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  CONSTRAINT `user_role_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_role`
--

LOCK TABLES `user_role` WRITE;
/*!40000 ALTER TABLE `user_role` DISABLE KEYS */;
INSERT INTO `user_role` VALUES (1,1,3,'2018-07-03 00:17:01'),(2,2,2,'2018-07-03 00:17:01'),(3,3,1,'2018-07-03 00:17:01'),(4,4,3,'2018-07-03 00:17:01'),(5,5,2,'2018-07-03 00:17:01'),(6,6,1,'2018-07-03 00:17:01');
/*!40000 ALTER TABLE `user_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_skill`
--

DROP TABLE IF EXISTS `user_skill`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `user_skill` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `skill_type_id` int(10) unsigned NOT NULL,
  `certificate` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `verified` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'NO',
  `creation_timestamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `skill_type_id` (`skill_type_id`),
  CONSTRAINT `user_skill_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  CONSTRAINT `user_skill_ibfk_2` FOREIGN KEY (`skill_type_id`) REFERENCES `skill` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_skill`
--

LOCK TABLES `user_skill` WRITE;
/*!40000 ALTER TABLE `user_skill` DISABLE KEYS */;
INSERT INTO `user_skill` VALUES (1,1,1,NULL,'NO','2018-07-03 00:17:06'),(2,1,5,NULL,'NO','2018-07-03 00:17:06'),(3,1,24,NULL,'NO','2018-07-03 00:17:06'),(4,1,115,NULL,'NO','2018-07-03 00:17:06'),(5,2,1,NULL,'NO','2018-07-03 00:17:06'),(6,2,5,NULL,'NO','2018-07-03 00:17:07'),(7,4,12,NULL,'NO','2018-07-03 00:17:07'),(8,4,8,NULL,'NO','2018-07-03 00:17:07'),(9,5,16,NULL,'NO','2018-07-03 00:17:07'),(10,6,3,NULL,'NO','2018-07-03 00:17:07'),(11,6,12,NULL,'NO','2018-07-03 00:17:07'),(12,6,8,NULL,'NO','2018-07-03 00:17:07'),(13,7,12,NULL,'NO','2018-07-03 00:17:07'),(14,8,12,NULL,'NO','2018-07-03 00:17:07'),(15,8,8,NULL,'NO','2018-07-03 00:17:07'),(16,9,8,NULL,'NO','2018-07-03 00:17:07'),(17,3,12,NULL,'NO','2018-07-03 00:17:07'),(18,3,1,NULL,'NO','2018-07-03 00:17:07'),(19,12,3,NULL,'NO','2018-07-03 00:17:07'),(20,13,16,NULL,'NO','2018-07-03 00:17:07'),(21,14,19,NULL,'NO','2018-07-03 00:17:07'),(22,15,5,NULL,'NO','2018-07-03 00:17:07');
/*!40000 ALTER TABLE `user_skill` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-07-14 17:04:37
