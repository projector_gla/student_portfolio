var databaseUtils = require('./utils/databaseUtils');
var redisUtils = require('./utils/redisUtils');
var logger = require('./logger');
var Constants = require('./constants');
var fs = require('fs');

function storeSkillListInRedis() {
  var categoryQuery = 'select id,skill_name,skill_type from skill';
  databaseUtils.executePlainQuery(categoryQuery, function(err, response) {
      if(err) {
        
          logger.logError(err);
      } else {
          var rslt=[];
          for(var i in response){
            var ele = response[i].skill_name + " in " + response[i].skill_type;
            rslt.push(ele);
          } 
          redisUtils.setItem(Constants.redisDataKeys.SKILL_LIST, rslt);
      }
  })


}
storeSkillListInRedis();
