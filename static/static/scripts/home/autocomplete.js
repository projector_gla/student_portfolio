
function autocomplete(inp, arr) {
/*the autocomplete function takes two arguments,
the text field element and an array of possible autocompleted values:*/
var currentFocus;
/*execute a function when someone writes in the text field:*/
inp.addEventListener("input", function(e) {
var a, b, i, val = this.value;
/*close any already open lists of autocompleted values*/
closeAllLists();
if (!val) { return false;}
currentFocus = -1;
/*create a DIV element that will contain the items (values):*/
a = document.createElement("DIV");
a.setAttribute("id", this.id + "autocomplete-list");
a.setAttribute("class", "autocomplete-items");
/*append the DIV element as a child of the autocomplete container:*/
this.parentNode.appendChild(a);
/*for each item in the array...*/
for (i = 0; i < arr.length; i++) {
/*check if the item starts with the same letters as the text field value:*/
if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
/*create a DIV element for each matching element:*/
b = document.createElement("DIV");
/*make the matching letters bold:*/
b.innerHTML = "<strong>" + arr[i].substr(0, val.length) + "</strong>";
b.innerHTML += arr[i].substr(val.length);
/*insert a input field that will hold the current array item's value:*/
b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
/*execute a function when someone clicks on the item value (DIV element):*/
b.addEventListener("click", function(e) {
/*insert the value for the autocomplete text field:*/
inp.value = this.getElementsByTagName("input")[0].value;
/*close the list of autocompleted values,
(or any other open lists of autocompleted values:*/
closeAllLists();
});
a.appendChild(b);
}
}
});
/*execute a function presses a key on the keyboard:*/
inp.addEventListener("keydown", function(e) {
var x = document.getElementById(this.id + "autocomplete-list");
if (x) x = x.getElementsByTagName("div");
if (e.keyCode == 40) {
/*If the arrow DOWN key is pressed,
increase the currentFocus variable:*/
currentFocus++;
/*and and make the current item more visible:*/
addActive(x);
} else if (e.keyCode == 38) { //up
/*If the arrow UP key is pressed,
decrease the currentFocus variable:*/
currentFocus--;
/*and and make the current item more visible:*/
addActive(x);
} else if (e.keyCode == 13) {
/*If the ENTER key is pressed, prevent the form from being submitted,*/
e.preventDefault();
if (currentFocus > -1) {
/*and simulate a click on the "active" item:*/
if (x) x[currentFocus].click();
}
}
});
function addActive(x) {
/*a function to classify an item as "active":*/
if (!x) return false;
/*start by removing the "active" class on all items:*/
removeActive(x);
if (currentFocus >= x.length) currentFocus = 0;
if (currentFocus < 0) currentFocus = (x.length - 1);
/*add class "autocomplete-active":*/
x[currentFocus].classList.add("autocomplete-active");
}
function removeActive(x) {
/*a function to remove the "active" class from all autocomplete items:*/
for (var i = 0; i < x.length; i++) {
x[i].classList.remove("autocomplete-active");
}
}
function closeAllLists(elmnt) {
/*close all autocomplete lists in the document,
except the one passed as an argument:*/
var x = document.getElementsByClassName("autocomplete-items");
for (var i = 0; i < x.length; i++) {
if (elmnt != x[i] && elmnt != inp) {
x[i].parentNode.removeChild(x[i]);
}
}
}
/*execute a function when someone clicks in the document:*/
document.addEventListener("click", function (e) {
closeAllLists(e.target);
});
}

/*An array containing all the country names in the world:*/
var skill = ["C","C++","JAVA","Python","C#","PHP","R","BASIS",".Net","JavaScript","Kotlin","Perl","Pascal","SQL","PL/SQL","UML","HTML","CSS","XML","Ruby","Painting",
"Dancing",
"Photography",
"Singing",
"Acting",
"Writing",
"Crafting","Cricket",
"Volleyball",
"Basketball",
"Chess",
"Swimming",
"Cycling",
"Hockey",
"Badminton",
"Tennis",
"Table Tennis",
"Wrestling",
"Football",
"Boxing",
"Golf",
"Archery",
"Athletics",
"Gymnastics",
"Handball",
"Kabaddi",
"Throwball",
"Weightlifting",
"Karate",
"Kho-kho","Graphic Design",
"Writing",
"Archeaology",
"History",
"Law",
"Politics",
"Literature",
"Religion",
"Philosophy",
"Linguistics",
"Intellectual Property",
"Culture studies",
"Soft skills",
"Educational Leadership",
"Development of Soclology",
"Visual perception",
"Tranformation and Lives","Community pharmacy",
"Hospital pharmacy",
"Clinical pharmacy",
"Industrial pharmacy",
"Regulatory pharmacy",
"Home care pharmacy",
"Managed care pharmacy",
"Research pharmacy",
"Oncology pharmacy",
"Ambulatory care pharmacy",
"Compounding pharmacy",
"Consultant pharmacy",
"Internet pharmacy",
"Veterinary pharmacy",
"Nuclear pharmacy",
"Military pharmacy",
"Specialty pharmacy",
"Geriatric pharmacy",
"Psychopharmacs_nameapy",
"Personal pharmacy",
"Nutritional support pharmacy",
"Hospice pharmacy",
"Pediatric pharmacy",
"Pharmacy benefit manager",
"Poison control pharmacy","Logistic Management",
"Marketing",
"Human Resource Development",
"Finance",
"Infromation System",
"Consulting",
"Entrepreneurship",
"Supply Chain Management",
"Leadership",
"Economic Growth and Development",
"Innovation",
"Business Models",
"Simulation of Business System",
"Sustainability through Green Manufacturing System",
"Total Quality Management",
"Microeconomics",
"Engineering Economics",
"Project management for managers",
"E-Business",
"Capital Management",
"Management of Inventory Systems","RAC Product Design",
"Laws of Thermodynamics",
"Smart Materials and Intelligent System Design",
"Surrogates and Approximations in Engineering Design",
"Refrigeration And Air-conditioning",
"Fluid dynamics and turbomachines",
"Principles of Metal Forming s_name",
"Principles of Casting s_name",
"Design for Quality, Manufacturing and Assembly",
"Mechanics of Machining",
"Manufacturing of Composite",
"Robotics",
"Principle of Hydraulic Machines and System Design",
"Abrasive Machining and Finishing Processes",
"Heat Exchangers",
"Energy Conservation and Waste Heat Recovery",
"Computational Fluid Mechanics",
"Manufacturing processes",
"Manufacturing Systems s_name",
"Noise Management and Control",
"Work System Design",
"Engineering fracture mechanics","Recent Advances in Transmission Insulators",
"Electrical Distribution System Analysis",
"Analysis and Design Principles of Microwave Antennas",
"Advanced Linear Continuous Control Systems",
"Discrete Time Signal Processing",
"Architectural Design of Digital Integrated Circuits",
"Physical Modelling for Electronics Enclosures using Rapid prototyping",
"Microwave Integrated Circuits",
"Power System Analysis",
"Basic Electrical Circuits",
"Analog circuits",
"Digital Circuits",
"Control Engineering",
"Principles of Signal Estimation",
"Microwave Theory and Techniques",
"Computational Electromagnetics & Applications",
"Principles of Digital Communications",
"Analog Communication",
"Wireless and Cellular Communications",
"Semiconductor Devices and Circuits",
"Design of Photovoltaic Systems",
"Fabrication Techniques",
"Op-Amp Applications: Design, Simulation and Implementation",
"Analog Electronic Circuit",
"Fiber-Optic Communication Systems and Techniques",
"Control System Design","Reinforced Concrete Road Bridges",
"Geosynthetics Testing Laboratory",
"Geotechnical Engineering Laboratory ",
"Principles of Construction Management",
"Matrix Method of Structural Analysis",
"Project planning and control",
"Fluid Inclusion in Mineral Principles and Application",
"Accounting and Finance for Civil Engineers",
"Foundation Engineering",
"Strength of Materials",
"Theory of Elasticity",
"Concrete s_name",
"Advanced Concrete s_name",
"Modern Construction materials",
"Design of Reinforced Concrete Structures",
"Integrated Waste Management for a Smart CityWastewater Treatment and Recycling",
"Glass in buildings : Design and applications",
"Glass Processing s_name",
"Environmental Engineering-Chemical Processes",
"Fire Protection",
"Services and Maintenance Management of Building",
"Geoenvironmental Engineering",
"Higher Surveying",
"Unsaturated Soil Mechanics",
"Application of Geotechnical Engineering","Computer Networking & Internet Protocols",
"Cyber Security",
"Database Management System",
"Software engineering",
"Website Design",
"Website Development",
"Deep Learning",
"Big Data",
"Computer Architecture",
"Cloud Computing",
"Data Analytics",
"Machine Learning",
"Artificial Intelligence",
"Data Mining",
"Data Science",
"Mobile Applications",
"Internet of Things",
"Microprocessor",
"Blockchain Architecture",
"Digital Image Processing","GNU Octave",
"ARDUINO",
"RASPBERRYPI",
"AUTOCAD",
"CAD",
"ANSYS",
"R STUDIO",
"CATIA",
"Adobe Photoshop",
"Patran",
"Calypso",
"MAYA",
"REVIT",
"TINKERCAD",
"3DS MAX",
"CIVIL 3D",
"GEO STUDIO",
"EAGLE",
"PLAXIX",
"ETABS",
"MAYA LT",
"MICROSTATION",
"CIVILDESIGNER",
"NETBEANS",
"JULIA",
"ECLIPS",
"ALLYCAD",
"BRIDGEDESIGNER",
"VISUAL STUDIO",
"MATHCAD",
"COMSOL",
"ABAQUS",
"SIMUFACT",
"RECAPPRO",
"PIPINGOFFICE",
"RHINOD",
"Chop",
"Gist",
"Bounce",
"Browsershots",
"CodePen",
"Dabblet",
"IE NetRenderer",
"JS Bin",
"JSFiddle",
"Web Design Tools",
"JSONLint",
"net2ftp",
"Liquid XML Studio",
"VP-UML",
"BlueJ"];

autocomplete(document.getElementById("myInput"), skill); 
