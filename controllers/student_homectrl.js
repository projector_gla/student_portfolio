var sessionUtils = require('../utils/sessionUtils');
var util=require('util');
var databaseUtils = require('./../utils/databaseUtils');
var redisUtils = require('./../utils/redisUtils');
var m = require("../main");
module.exports = {



    showstudent_homepage: function* (next) {

var current= this.currentUser.university_id;
var currentstr = 'select university_id from user where university_id ='+current ;
var currentresult = yield databaseUtils.executeQuery(currentstr);
var currentdetails = currentresult[0];

//name details

var namestr = 'select name from user where university_id ='+current ;
var nameresult = yield databaseUtils.executeQuery(namestr);
var namedetails = nameresult[0];


          //LIST OF TOPPERS:

          var top5Query = 'select user.name,user.email_id,user.university_id,user.phone,user.photo from user inner join user_qualification on user.id=user_qualification.user_id where user_qualification.qualification_id =2 order by user_qualification.percentage desc limit 5';
          var top5Result = yield databaseUtils.executeQuery(top5Query);
          var top5Details = top5Result[0];

       
          var u_id=this.currentUser.university_id;

          var id4='select id from user where university_id="%s"';
          var id3=util.format(id4,u_id);
          var id2=yield databaseUtils.executeQuery(id3);
          var id=id2[0].id;
console.log(id);
     
          //list of skills;
       
       var topskill1 ='select skill_name from skill inner join user_skill on skill.id=user_skill.skill_type_id where user_skill.user_id="%s"';
       var topskill2=util.format(topskill1,id);
       console.log(topskill2);
       
       var topskill3=yield databaseUtils.executeQuery(topskill2);
       var topskillresult=topskill3[0].id;



        yield this.render('student_home',{

            
            top5Details: top5Details,
            top5List: top5Result,

            currentdetails:currentdetails,
            currentlist:currentresult,

            namedetails: namedetails,
            namelist: nameresult,

            topskillresult: topskillresult,
            topskilllist: topskill3,

        });
    },

    studentsearchskill: function* (next) {
        var search=this.request.body.search;
         var val='/api/search?s='+ encodeURIComponent(search);
         
    this.redirect(val);
    },



    logout: function* (next)
    {
        var sessionId = this.cookies.get("SESSION_ID");
        
        if(sessionId)
            {
            sessionUtils.deleteSession(sessionId);
            }
        this.cookies.set("SESSION_ID", '', {expires: new Date(1), path: '/'});
        this.redirect('/api/home');
    }
}
