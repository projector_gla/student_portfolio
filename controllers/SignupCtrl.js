var sessionUtils = require('../utils/sessionUtils');
var util=require('util');
var databaseUtils = require('./../utils/databaseUtils');

module.exports = {

    signup: function* (next){
        yield this.render('signup',{
            
        });

    },



    signupPage: function* (next) {

        var university_id=this.request.body.university_id;
        var email_id=this.request.body.email_id;
        var password=this.request.body.password;
        
        var allid='select university_id from user';
        var query1 =util.format(allid);
        var result1 =yield databaseUtils.executeQuery(query1);
       
        var error=false;
        for ( var i in result1)
        {   
            
            if(result1[i].university_id===university_id)
            {
                error=true;
                break;
            }

        }
        if (error===false) {
        var queryString='insert into user(university_id,email_id,password) values("%s","%s","%s")';
        var query=util.format(queryString,university_id,email_id,password);
        var result=yield databaseUtils.executeQuery(query);

        var id4='select id from user where university_id="%s"';
        var id3=util.format(id4,university_id);
        var id2=yield databaseUtils.executeQuery(id3);
        var id=id2[0].id;

       
        var qualificationid1 ='select id from qualification order by id';
        var  qualificationid2=util.format(qualificationid1);
        var result2 =yield databaseUtils.executeQuery(qualificationid2);
       var j=0;

        for( var i in result2) 
        {        
        var queryString1='insert into user_qualification(user_id,qualification_id) values(%s,%s)';
        var query2=util.format(queryString1, id, j+1);
        var result2=yield databaseUtils.executeQuery(query2);
        j++;
       }       


        queryString='select * from user where id="%s"';
        query = util.format(queryString,result.insertId);
      
        result2=yield databaseUtils.executeQuery(query);
        sessionUtils.saveUserInSession(result2[0],this.cookies);
       
         var val='/api/student_home';
         this.redirect(val);    
         } else {
            
             var val='/api/home';
             this.redirect(val);      

        }

    },
    
}
