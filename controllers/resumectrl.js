var sessionUtils = require('../utils/sessionUtils');
var util=require('util');
var databaseUtils = require('./../utils/databaseUtils');

module.exports = {

   
    showresumePage: function* (next){

        var u_id=this.params.id;
        var id4='select id from user where university_id="%s"';
        var id3=util.format(id4,u_id);
        var id2=yield databaseUtils.executeQuery(id3);
        var id=id2[0].id;
    
              
var photostr = 'select photo from user where id="%s"';
var photoquery=util.format(photostr,id);
var photoresult = yield databaseUtils.executeQuery(photoquery);
var photodetails = photoresult[0];
    
        
var currentstr = 'select university_id from user where id="%s"';
var currentquery=util.format(currentstr,id);
var currentresult = yield databaseUtils.executeQuery(currentquery);
var currentdetails = currentresult[0];

var perquerystr = 'select user_qualification.percentage from user_qualification inner join user on user_qualification.user_id=user.id where user.id ="%s" and user_qualification.qualification_id=1';
var perquery=util.format(perquerystr,id);
var perresult = yield databaseUtils.executeQuery(perquery);
var perdetails = perresult[0];

var perquerystr11 = 'select user_qualification.percentage from user_qualification inner join user on user_qualification.user_id=user.id where user.id ="%s" and user_qualification.qualification_id=2';
var perquery11=util.format(perquerystr11,id);
var perresult11 = yield databaseUtils.executeQuery(perquery11);
var perdetails11 = perresult11[0];

var perquerystr1 = 'select user_qualification.percentage from user_qualification inner join user on user_qualification.user_id=user.id where user.id ="%s" and user_qualification.qualification_id=3';
var perquery1=util.format(perquerystr1,id);
var perresult1 = yield databaseUtils.executeQuery(perquery1);
var perdetails1 = perresult1[0];
      
var perquerystr2 = 'select user_qualification.percentage from user_qualification inner join user on user_qualification.user_id=user.id where user.id ="%s" and user_qualification.qualification_id=4';
var perquery2=util.format(perquerystr2,id);
var perresult2 = yield databaseUtils.executeQuery(perquery2);
var perdetails2 = perresult2[0];

var perquerystr8 = 'select user_qualification.percentage from user_qualification inner join user on user_qualification.user_id=user.id where user.id ="%s" and user_qualification.qualification_id=5';
var perquery8=util.format(perquerystr8,id);
var perresult8 = yield databaseUtils.executeQuery(perquery8);
var perdetails8 = perresult8[0];

var perquerystr12 = 'select user_qualification.passing_year from user_qualification inner join user on user_qualification.user_id=user.id where user.id ="%s" and user_qualification.qualification_id=1';
var perquery12=util.format(perquerystr12,id);
var perresult12 = yield databaseUtils.executeQuery(perquery12);
var perdetails12 = perresult12[0];

var perquerystr13 = 'select user_qualification.passing_year from user_qualification inner join user on user_qualification.user_id=user.id where user.id ="%s" and user_qualification.qualification_id=2';
var perquery13=util.format(perquerystr13,id);
var perresult13 = yield databaseUtils.executeQuery(perquery13);
var perdetails13 = perresult13[0];

var perquerystr14 = 'select user_qualification.passing_year from user_qualification inner join user on user_qualification.user_id=user.id where user.id ="%s" and user_qualification.qualification_id=3';
var perquery14=util.format(perquerystr14,id);
var perresult14 = yield databaseUtils.executeQuery(perquery14);
var perdetails14 = perresult14[0];


var perquerystr17 = 'select user_qualification.passing_year from user_qualification inner join user on user_qualification.user_id=user.id where user.id ="%s" and user_qualification.qualification_id=4';
var perquery17=util.format(perquerystr17,id);
var perresult17 = yield databaseUtils.executeQuery(perquery17);
var perdetails17 = perresult17[0];

var perquerystr21 = 'select user_qualification.passing_year from user_qualification inner join user on user_qualification.user_id=user.id where user.id ="%s" and user_qualification.qualification_id=5';
var perquery21=util.format(perquerystr21,id);
var perresult21 = yield databaseUtils.executeQuery(perquery21);
var perdetails21 = perresult21[0];

var perquerystr3 = 'select user_qualification.board from user_qualification inner join user on user_qualification.user_id=user.id where user.id ="%s" and user_qualification.qualification_id=1';
var perquery3=util.format(perquerystr3,id);
var perresult3 = yield databaseUtils.executeQuery(perquery3);
var perdetails3 = perresult3[0];

var perquerystr4 = 'select user_qualification.board from user_qualification inner join user on user_qualification.user_id=user.id where user.id ="%s" and user_qualification.qualification_id=2';
var perquery4=util.format(perquerystr4,id);
var perresult4 = yield databaseUtils.executeQuery(perquery4);
var perdetails4 = perresult4[0];
      
var perquerystr9 = 'select user_qualification.board from user_qualification inner join user on user_qualification.user_id=user.id where user.id ="%s" and user_qualification.qualification_id=3';
var perquery9=util.format(perquerystr9,id);
var perresult9 = yield databaseUtils.executeQuery(perquery9);
var perdetails9 = perresult9[0];


var perquerystr19 = 'select user_qualification.board from user_qualification inner join user on user_qualification.user_id=user.id where user.id ="%s" and user_qualification.qualification_id=4';
var perquery19=util.format(perquerystr19,id);
var perresult19 = yield databaseUtils.executeQuery(perquery19);
var perdetails19 = perresult19[0];

var perquerystr20 = 'select user_qualification.board from user_qualification inner join user on user_qualification.user_id=user.id where user.id ="%s" and user_qualification.qualification_id=5';
var perquery20=util.format(perquerystr20,id);
var perresult20 = yield databaseUtils.executeQuery(perquery20);
var perdetails20 = perresult20[0];

var perquerystr5 = 'select name from user where id="%s"';
var perquery5=util.format(perquerystr5,id);
var perresult5 = yield databaseUtils.executeQuery(perquery5);
var perdetails5 = perresult5[0];

var perquerystr6 = 'select phone from user where id="%s"';
var perquery6=util.format(perquerystr6,id);
var perresult6 = yield databaseUtils.executeQuery(perquery6);
var perdetails6 = perresult6[0];

var perquerystr10 = 'select university_id from user where id="%s"';
var perquery10=util.format(perquerystr10,id);
var perresult10 = yield databaseUtils.executeQuery(perquery10);
var perdetails10 = perresult10[0];

var perquerystr7 = 'select email_id from user where id="%s"';
var perquery7=util.format(perquerystr7,id);
var perresult7 = yield databaseUtils.executeQuery(perquery7);
var perdetails7 = perresult7[0];

var perquerystr15 = 'select skill_type,skill_name,verified from user inner join user_skill on user.id=user_skill.user_id inner join skill on user_skill.skill_type_id= skill.id where user.id="%s";'
var perquery15=util.format(perquerystr15,id);
var perresult15 = yield databaseUtils.executeQuery(perquery15);
var perdetails15 = perresult15;

var perquerystr16 = 'select user_qualification.creation_timestamp from user_qualification inner join user on user_qualification.user_id=user.id where user.id ="%s" and user_qualification.qualification_id=3';
var perquery16=util.format(perquerystr16,id);
var perresult16 = yield databaseUtils.executeQuery(perquery16);
var perdetails16 = perresult16[0];



        yield this.render('resume',{
            
            
            photodetails: photodetails,
            photolist: photoresult,

            currentdetails: currentdetails,
            currentlist: currentresult,
  
            perdetails: perdetails,
            perlist: perresult,
  
            perdetails1: perdetails1,
            perlist1: perresult1,
  
            perdetails2: perdetails2,
            perlist2: perresult2,

            perdetails3: perdetails3,
            perlist3: perresult3,

            perdetails4: perdetails4,
            perlist4: perresult4,

            perdetails5: perdetails5,
            perlist5: perresult5,

            perdetails6: perdetails6,
            perlist6: perresult6,

            perdetails7: perdetails7,
            perlist7: perresult7,

            perdetails8: perdetails8,
            perlist8: perresult8,

            perdetails12: perdetails12,
            perlist12: perresult12,

            perdetails9: perdetails9,
            perlist9: perresult9,

            perdetails10: perdetails10,
            perlist10: perresult10,

            perdetails11: perdetails11,
            perlist11: perresult11,

            perdetails13: perdetails13,
            perlist13: perresult13,

            perdetails14: perdetails14,
            perlist14: perresult14,

            perdetails15: perdetails15,
            perlist15: perresult15,

            perdetails16: perdetails16,
            perlist16: perresult16,

            
            perdetails17: perdetails17,
            perlist17: perresult17,

            perdetails19: perdetails19,
            perlist19: perresult19,
  
            perdetails20: perdetails20,
            perlist20: perresult20,
  
            perdetails21: perdetails21,
            perlist21: perresult21
  
        });


    },
}
