var sessionUtils = require('../utils/sessionUtils');
var util=require('util');
var databaseUtils = require('./../utils/databaseUtils');

module.exports = {

    showSearchPage: function* (next) {

        var course=this.request.query.course;
        var dep=this.request.query.dep;
                 
if(dep && course){

        var user = 'select distinct user.* from user inner join user_qualification on user.id=user_qualification.user_id inner join course on course.id=user_qualification.course_id inner join department on department.id=user_qualification.department_id inner join course_department on course_department.department_id=department.id where course_department.course_id in (select course.id where course.course_name="%s") and course_department.department_id in (select department.id where department.department_name="%s")';
        var userquery = util.format(user,course,dep);
        var userResult = yield databaseUtils.executeQuery(userquery);
        var userDetails3=userResult[0];

}
else if(course)
{
        var dept = 'select distinct user.* from user inner join user_qualification on user.id=user_qualification.user_id inner join course on course.id=user_qualification.course_id where course.course_name="%s"';
        var deptquery = util.format(dept,course);
        var deptResult = yield databaseUtils.executeQuery(deptquery);
        var userDetails2=deptResult[0];
}
else if(dep)
{
        var depa = 'select user.* from user inner join user_qualification on user.id=user_qualification.user_id inner join department on department.id=user_qualification.department_id where department.department_name="%s"';
        var depaquery = util.format(depa,dep);
        var depaResult = yield databaseUtils.executeQuery(depaquery);
        var userDetails5=depaResult[0];
}
       //LIST OF STUDENTS IN PARTICULAR YEAR:
        
        var y=this.request.query.year;
        var year = 'select distinct university_id,name,email_id,photo,phone from user where ((select substring((select year(curdate())),3,2)) - (select substring((select university_id),1,2))) ="%s"';
        var yearquery = util.format(year,y);
        var yearResult = yield databaseUtils.executeQuery(yearquery);
        var userDetails4=yearResult[0];

       
        //LIST OF STUDENTS in multiple skill :
        

        var count=1;
        var m= this.request.query.skill_name;
        var stringsearch = ",",str = m;

    if(str!=null)
     {
        for(var i=0; i<str.length; count+=+(stringsearch===str[i++]));
     
        
        var res = m.replace(/,/g,"\",\"");
        var multipleskillquery= 'select distinct user.name, user.university_id, user.photo, user.email_id,user.phone from user inner join user_skill on user.id=user_skill.user_id inner join skill on user_skill.skill_type_id=skill.id where skill.skill_name in("%s") group by user.id having count(user.id)>="%s"';
        var multipleskillResult = util.format(multipleskillquery,res,count);
       
        var name = yield databaseUtils.executeQuery(multipleskillResult);
        var userDetail =name[0];
             }

        //LIST OF STUDENTS:
       var student =this.request.query.s;
       var userQueryStr = 'select distinct user.university_id, user.name, user.photo, user.email_id,user.phone from skill left join user_skill on skill.id = user_skill.skill_type_id left join user on user.id = user_skill.user_id where skill.skill_name ="%s"';
       var userQuery = util.format(userQueryStr,student);
       var userResult1 = yield databaseUtils.executeQuery(userQuery);
       var userDetails1 = userResult1[0];

  
  
        yield this.render('search',{
        
            
                  userDetail:userDetail,
                  multipleList:name,

                  userDetails1: userDetails1,
                  userList1: userResult1,

                  userDetails2: userDetails2, 
                  deptList:deptResult,

                  userDetails3: userDetails3, 
                  userList:userResult,

                  userDetails4: userDetails4, 
                  yearList:yearResult,

                  userDetails5: userDetails5, 
                  depaList:depaResult


        });

    },

    multiplesearchskill: function* (next) {
        var search=this.request.body.skill_name;
         var val='/api/search?skill_name='+ encodeURIComponent(search);
         
    this.redirect(val);
    },

   }
