var sessionUtils = require('../utils/sessionUtils');
var util=require('util');
var databaseUtils = require('./../utils/databaseUtils');

module.exports = {



verifyLoginPage: function* (next) {
var university_id = this.request.body.university_id;
var password = this.request.body.password;

var querystring = 'select university_id,name, password from user where university_id = "%s"';
var query = util.format(querystring,university_id);
var name = yield databaseUtils.executeQuery(query);
var userDetails=name[0];
var count =university_id.length;

if ((userDetails.password===password)&&(userDetails.university_id===university_id)&&(count===9))
{
    sessionUtils.saveUserInSession(userDetails,this.cookies);
    var val='/api/student_home';
    this.redirect(val);
}

else if ((userDetails.password===password)&&(userDetails.university_id===university_id)&&(count===6))
{
    sessionUtils.saveUserInSession(userDetails,this.cookies);
    var val='/api/teacherhome/';
    this.redirect(val);
}

else{
this.redirect('/api/home');   
}

},  
  
verify: function* (next) {
    var university_id = this.request.body.university_id;
    var query=yield databaseUtils.executeQuery(util.format('select * from user where university_id="%s"',university_id));
    if(query.length==0) this.body={flag:'0'};
    else this.body={flag:'1'};
},
verifypassword: function* (next) {
    var password = this.request.body.password;
    var university_id = this.request.body.university_id;
    var query=yield databaseUtils.executeQuery(util.format('select password from user where university_id="%s"',university_id));
    if(password===query[0].password) this.body={flag:'0'};
    else this.body={flag:'1'};
}


}
