var sessionUtils = require('../utils/sessionUtils');
var util=require('util');
var databaseUtils = require('./../utils/databaseUtils');
module.exports = {

    resumePage: function* (next)
     {

        //id 

        var u_id=this.currentUser.university_id;

        var id4='select id from user where university_id="%s"';
        var id3=util.format(id4,u_id);
        var id2=yield databaseUtils.executeQuery(id3);
        var id=id2[0].id;
        //console.log(id);
                     
//personal details

var rname=this.request.body.fields.rname;
var rphone=this.request.body.fields.rphone;
var remail=this.request.body.fields.remail;
        

var iname=this.request.body.files;
this.body=iname;
var url=this.body.imgname.path;
var isize=this.body.imgname.size;
var appurl=url.replace(/\\/g,"/");
var appurl1=appurl.replace(/static\/static/g,'static');

        if (isize){
            var queryString='update user set name="%s", phone=%s ,email_id="%s",photo="%s" where id=%s ';
            var query=util.format(queryString,rname,rphone,remail,appurl1,id);
            var result=yield databaseUtils.executeQuery(query);    
        }

        else{
        var queryString='update user set name="%s", phone=%s ,email_id="%s" where id=%s ';
        var query=util.format(queryString,rname,rphone,remail,id);
        var result=yield databaseUtils.executeQuery(query);
        }
//console.log("user updated");

        //10

        var passing_year1=this.request.body.fields.passing_year;
        var board1=this.request.body.fields.board;
        var percentage1=this.request.body.fields.percentage;

        var iname1=this.request.body.files;
        this.body=iname1;
        var url1=this.body.img10.path;
        var isize1=this.body.img10.size;
        var img10url=url1.replace(/\\/g,"/");
        var report10url=img10url.replace(/static\/static/g,'static');

if(isize1){

        var queryString='update user_qualification set passing_year=%s, board="%s", percentage=%s,report_card="%s" where user_id=%s and qualification_id=1';
        var query=util.format(queryString,passing_year1,board1,percentage1,report10url,id);
        var result10=yield databaseUtils.executeQuery(query);   
       // console.log("if");
        
}
else{
 
    var queryString='update user_qualification set passing_year=%s, board="%s", percentage=%s where user_id=%s and qualification_id=1';
    var query=util.format(queryString,passing_year1,board1,percentage1,id);
    var result10=yield databaseUtils.executeQuery(query);
    
}


       //12

         var passing_year2=this.request.body.fields.passing_year1;
         var board2=this.request.body.fields.board1;
         var percentage2=this.request.body.fields.percentage1;
   
            var iname2=this.request.body.files;
         this.body=iname2;
         var url2=this.body.img12.path;
         var isize2=this.body.img12.size;
         var img12url=url2.replace(/\\/g,"/");
         var report12url=img12url.replace(/static\/static/g,'static');

         if(isize2){

        var queryString1='update user_qualification set passing_year="%s", board="%s", percentage="%s",report_card="%s" where user_id= "%s" and qualification_id=2';
         var query1=util.format(queryString1,passing_year2,board2,percentage2,report12url,id);
         var result=yield databaseUtils.executeQuery(query1);
         }
         else{
            
         var queryString1='update user_qualification set passing_year="%s", board="%s", percentage="%s" where user_id= "%s" and qualification_id=2';
         var query1=util.format(queryString1,passing_year2,board2,percentage2,id);
         var result=yield databaseUtils.executeQuery(query1);
         }
/*
        //ug
        
         var passing_year3=this.request.body.fields.passing_year2;
         var board3=this.request.body.fields.board2;
         var percentage3=this.request.body.fields.percentage2;
         
        var courseug=this.request.body.fields.courseug;
       // console.log(courseug);
        
        
        var departmentug=this.request.body.fields.departmentug;


         var iname3=this.request.body.files;
         this.body=iname3;
         var url3=this.body.imgug.path;
         var isize3=this.body.imgug.size;
         var imgugurl=url3.replace(/\\/g,"/");
         var reportugurl=imgugurl.replace(/static\/static/g,'static');
       
         if(isize3)
         {

            var ugquery1='update user_qualification set qualification_id=((select qualification_id from course where course_name="%s")),course_id=((select id from course where course_name="%s")),department_id=((select id from department where department_name="%s")) where user_id="%s" and qualification_id=((select qualification_id from course where course_name="%s"));';
            var ugquery2=util.format(ugquery1,courseug,courseug,departmentug,id,courseug);
            var resultug=yield databaseUtils.executeQuery(ugquery2);    

            var queryugid='select qualification_id from course where course_name="%s"';
            var queryugid2=util.format(queryugid,courseug);
            var queryugid3=yield databaseUtils.executeQuery(queryugid2);
            var queryugid4=queryugid3[0].qualification_id;    

         var queryString2='update user_qualification set passing_year="%s", board="%s", percentage="%s",report_card="%s" where user_id= "%s" and qualification_id="%s"';
          var query2=util.format(queryString2,passing_year3,board3,percentage3,reportugurl,id,queryugid4);
          var resultug1=yield databaseUtils.executeQuery(query2);
         }
         else{

            var ugquery1='update user_qualification set qualification_id=((select qualification_id from course where course_name="%s")),course_id=((select id from course where course_name="%s")),department_id=((select id from department where department_name="%s")) where user_id="%s" and qualification_id=((select qualification_id from course where course_name="%s"));';
            var ugquery2=util.format(ugquery1,courseug,courseug,departmentug,id,courseug);
            var resultug=yield databaseUtils.executeQuery(ugquery2);    

            var queryugid='select qualification_id from course where course_name="%s"';
            var queryugid2=util.format(queryugid,courseug);
            var queryugid3=yield databaseUtils.executeQuery(queryugid2);
            var queryugid4=queryugid3[0].qualification_id;    

         var queryString2='update user_qualification set passing_year="%s", board="%s", percentage="%s" where user_id= "%s" and qualification_id="%s"';
          var query2=util.format(queryString2,passing_year3,board3,percentage3,id,queryugid4);
          var resultug1=yield databaseUtils.executeQuery(query2);
            }

 
         
*/

         
/*
        //post graduation
        
        var passing_year4=this.request.body.fields.passing_year3;
        var board4=this.request.body.fields.board3;
        var percentage4=this.request.body.fields.percentage3;
        var iname4=this.request.body.files;        
        this.body=iname4;
        var url4=this.body.imgpg.path;
        var isize4=this.body.imgpg.size;
        var imgpgurl=url4.replace(/\\/g,"/");
        var reportpgurl=imgpgurl.replace(/static\/static/g,'static');
       

        if(isize4)
        {
        var queryString3='update user_qualification set passing_year="%s", board="%s", percentage="%s",report_card="%s",course_id="%s", department_id="%s" where user_id= "%s" and qualification_id="%s" ';
        var query3=util.format(queryString3,passing_year4,board4,percentage4,reportpgurl,1,1,id,4);
        var result=yield databaseUtils.executeQuery(query3);
        }
        else{

            var queryString3='update user_qualification set passing_year="%s", board="%s", percentage="%s",course_id="%s", department_id="%s" where user_id= "%s" and qualification_id="%s" ';
            var query3=util.format(queryString3,passing_year4,board4,percentage4,1,1,id,4);
            var result=yield databaseUtils.executeQuery(query3);       
        }
        
              //phd
        
        var passing_year5=this.request.body.fields.passing_year2;
        var board5=this.request.body.fields.board2;
        var percentage5=this.request.body.fields.percentage2;

        var iname5=this.request.body.files;
        this.body=iname5;
        var url5=this.body.imgphd.path;
        var isize5=this.body.imgphd.size;
        var imgphdurl=url5.replace(/\\/g,"/");
        var reportphdurl=imgphdurl.replace(/static\/static/g,'static');
       
        if(isize5)
        {
        var queryString4='update user_qualification set passing_year="%s", board="%s", percentage="%s",report_card="%s",course_id="%s", department_id="%s" where user_id= "%s" and qualification_id="%s" ';
        var query4=util.format(queryString4,passing_year5,board5,percentage5,reportphdurl,1,1,id,5);
        var result=yield databaseUtils.executeQuery(query4);
        }
        else{

            var queryString4='update user_qualification set passing_year="%s", board="%s", percentage="%s",course_id="%s", department_id="%s" where user_id= "%s" and qualification_id="%s" ';
            var query4=util.format(queryString4,passing_year4,board4,percentage4,1,1,id,5);
            var result=yield databaseUtils.executeQuery(query4);       
        }
  */      

       // skills

       var skills=this.request.body.fields.skill;
    console.log(skills);


      if (skills !==undefined)
      {
       var skillname ='select id from skill where skill_name="%s"';
       var skillque=util.format(skillname,skills);
       var result=yield databaseUtils.executeQuery(skillque);
       var skill1 =result[0].id;

       
       var insertskill='insert into user_skill (skill_type_id,user_id) values ("%s","%s")';
       var insertquery=util.format(insertskill,skill1,id);
       var insertresult=yield databaseUtils.executeQuery(insertquery);

          }
        
      
        var rr =this.currentUser.university_id;
        var val='/app/resume/'+rr;
        this.redirect(val);
      
    },

    resume: function* (next){

        var u_id=this.currentUser.university_id;
        var id4='select id from user where university_id="%s"';
        var id3=util.format(id4,u_id);
        var id2=yield databaseUtils.executeQuery(id3);
        var id=id2[0].id;
    
                      
var photostr = 'select photo from user where id="%s"';
var photoquery=util.format(photostr,id);
var photoresult = yield databaseUtils.executeQuery(photoquery);
var photodetails = photoresult[0];

//personal details

var perquerystr5 = 'select name from user where id="%s"';
var perquery5=util.format(perquerystr5,id);
var perresult5 = yield databaseUtils.executeQuery(perquery5);
var perdetails5 = perresult5[0];

var perquerystr3 = 'select university_id from user where id="%s"';
var perquery3=util.format(perquerystr3,id);
var perresult3 = yield databaseUtils.executeQuery(perquery3);
var perdetails3 = perresult3[0];

var perquerystr4 = 'select email_id from user where id="%s"';
var perquery4=util.format(perquerystr4,id);
var perresult4 = yield databaseUtils.executeQuery(perquery4);
var perdetails4 = perresult4[0];

var perquerystr6 = 'select phone from user where id="%s"';
var perquery6=util.format(perquerystr6,id);
var perresult6 = yield databaseUtils.executeQuery(perquery6);
var perdetails6 = perresult6[0];


 //10th percentage               
 var perquerystr = 'select user_qualification.percentage from user_qualification inner join user on user_qualification.user_id=user.id where user.id ="%s" and user_qualification.qualification_id=1';
 var perquery=util.format(perquerystr,id);
 var perresult = yield databaseUtils.executeQuery(perquery);
 var perdetails = perresult[0];
 
 
//10th board
var perquerystr7 = 'select user_qualification.board from user_qualification inner join user on user_qualification.user_id=user.id where user.id ="%s" and user_qualification.qualification_id=1';
var perquery7=util.format(perquerystr7,id);
var perresult7 = yield databaseUtils.executeQuery(perquery7);
var perdetails7 = perresult7[0];

//10th passing year
var perquerystr10 = 'select user_qualification.passing_year from user_qualification inner join user on user_qualification.user_id=user.id where user.id ="%s" and user_qualification.qualification_id=1';
var perquery10=util.format(perquerystr10,id);
var perresult10 = yield databaseUtils.executeQuery(perquery10);
var perdetails10 = perresult10[0];



//10th report card
var perquerystr14 = 'select user_qualification.report_card from user_qualification inner join user on user_qualification.user_id=user.id where user.id ="%s" and user_qualification.qualification_id=1';
var perquery14=util.format(perquerystr14,id);
var perresult14 = yield databaseUtils.executeQuery(perquery14);
var perdetails14 = perresult14[0];





 //12th percentage      
 var perquerystr1 = 'select user_qualification.percentage from user_qualification inner join user on user_qualification.user_id=user.id where user.id ="%s" and user_qualification.qualification_id=2';
 var perquery1=util.format(perquerystr1,id);
 var perresult1 = yield databaseUtils.executeQuery(perquery1);
 var perdetails1 = perresult1[0];

//12th board
var perquerystr8 = 'select user_qualification.board from user_qualification inner join user on user_qualification.user_id=user.id where user.id ="%s" and user_qualification.qualification_id=2';
var perquery8=util.format(perquerystr8,id);
var perresult8 = yield databaseUtils.executeQuery(perquery8);
var perdetails8 = perresult8[0];

//12th passing year
var perquerystr11 = 'select user_qualification.passing_year from user_qualification inner join user on user_qualification.user_id=user.id where user.id ="%s" and user_qualification.qualification_id=2';
var perquery11=util.format(perquerystr11,id);
var perresult11 = yield databaseUtils.executeQuery(perquery11);
var perdetails11 = perresult11[0];


//12th report card
var perquerystr15 = 'select user_qualification.report_card from user_qualification inner join user on user_qualification.user_id=user.id where user.id ="%s" and user_qualification.qualification_id=2';
var perquery15=util.format(perquerystr15,id);
var perresult15 = yield databaseUtils.executeQuery(perquery15);
var perdetails15 = perresult15[0];



/*


 //graduation percentage
 var perquerystr2 = 'select user_qualification.percentage from user_qualification inner join user on user_qualification.user_id=user.id where user.id ="%s" and user_qualification.qualification_id=3';
 var perquery2=util.format(perquerystr2,id);
 var perresult2 = yield databaseUtils.executeQuery(perquery2);
 var perdetails2 = perresult2[0];

//graduation board
var perquerystr9 = 'select user_qualification.board from user_qualification inner join user on user_qualification.user_id=user.id where user.id ="%s" and user_qualification.qualification_id=3';
var perquery9=util.format(perquerystr9,id);
var perresult9 = yield databaseUtils.executeQuery(perquery9);
var perdetails9 = perresult9[0];

//graduation passing year
var perquerystr12 = 'select user_qualification.passing_year from user_qualification inner join user on user_qualification.user_id=user.id where user.id ="%s" and user_qualification.qualification_id=3';
var perquery12=util.format(perquerystr12,id);
var perresult12 = yield databaseUtils.executeQuery(perquery12);
var perdetails12 = perresult12[0];

//grad report card
var perquerystr16 = 'select user_qualification.report_card from user_qualification inner join user on user_qualification.user_id=user.id where user.id ="%s" and user_qualification.qualification_id=3';
var perquery16=util.format(perquerystr16,id);
var perresult16 = yield databaseUtils.executeQuery(perquery16);
var perdetails16 = perresult16[0];

//graduation course
var perquerystrc = 'select course_name from course where id = ((select max(course_id) from user_qualification where user_id="%s"))';
var perqueryc=util.format(perquerystrc,id);
var perresultc = yield databaseUtils.executeQuery(perqueryc);
var perdetailsc = perresultc[0];
//console.log(perdetailsc);

//graduation department
var perquerystrd = 'select department_name from department where id = ((select max(department_id) from user_qualification where user_id="%s" and course_id=((select max(course_id) from user_qualification where user_id="%s"))))';
var perqueryd=util.format(perquerystrd,id,id);
//console.log(perqueryd);

var perresultd = yield databaseUtils.executeQuery(perqueryd);
var perdetailsd = perresultd[0];
//console.log(perdetailsd);


//pg report card
var perquerystr16 = 'select user_qualification.report_card from user_qualification inner join user on user_qualification.user_id=user.id where user.id ="%s" and user_qualification.qualification_id=3';
var perquery16=util.format(perquerystr16,id);
var perresult16 = yield databaseUtils.executeQuery(perquery16);
var perdetails16 = perresult16[0];

//phd report card
var perquerystr16 = 'select user_qualification.report_card from user_qualification inner join user on user_qualification.user_id=user.id where user.id ="%s" and user_qualification.qualification_id=3';
var perquery16=util.format(perquerystr16,id);
var perresult16 = yield databaseUtils.executeQuery(perquery16);
var perdetails16 = perresult16[0];

*/
//skills
var perquerystr13 = 'select skill_type,skill_name,verified from user inner join user_skill on user.id=user_skill.user_id inner join skill on user_skill.skill_type_id = skill.id where user.id="%s"';
var perquery13=util.format(perquerystr13,id);
var perresult13 = yield databaseUtils.executeQuery(perquery13);
var perdetails13 = perresult13;
console.log(perdetails13);



        yield this.render('update',{

            
            photodetails: photodetails,
            photolist: photoresult,

            perdetails: perdetails,
            perlist: perresult,

            
            // perdetailsc: perdetailsc,
            // perlistc: perresultc,

            
            // perdetailsd: perdetailsd,
            // perlistd: perresultd,

            perdetails5: perdetails5,
            perlist5: perresult5,

            perdetails3: perdetails3,
            perlist3: perresult3,
  
            perdetails1: perdetails1,
            perlist1: perresult1,

            perdetails4: perdetails4,
            perlist4: perresult4,

            perdetails6: perdetails6,
            perlist6: perresult6,

            perdetails7: perdetails7,
            perlist7: perresult7,

            perdetails8: perdetails8,
            perlist8: perresult8,

            // perdetails9: perdetails9,
            // perlist9: perresult9,

            perdetails10: perdetails10,
            perlist10: perresult10,

            perdetails11: perdetails11,
            perlist11: perresult11,

            // perdetails12: perdetails12,
            // perlist12: perresult12,
  
            perdetails13: perdetails13,
            perlist13: perresult13,

            perdetails14: perdetails14,
            perlist14: perresult14,

            perdetails15: perdetails15,
            perlist15: perresult15,

            // perdetails16: perdetails16,
            // perlist16: perresult16,

            // perdetails2: perdetails2,
            // perlist2: perresult2
 

           
            
        });

    },

    
    
    
    logout: function* (next) 
    {
        var sessionId = this.cookies.get("SESSION_ID");
        if(sessionId)
            {
            sessionUtils.deleteSession(sessionId);
            }
        this.cookies.set("SESSION_ID", '', {expires: new Date(1), path: '/'});

        this.redirect('/');
    }
}