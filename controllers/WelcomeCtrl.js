var sessionUtils = require('../utils/sessionUtils');
var util=require('util');
var databaseUtils = require('./../utils/databaseUtils');
var redisUtils = require('./../utils/redisUtils');
var m = require("../main");
module.exports = {



    showHomePage: function* (next) {


          //LIST OF TOPPERS:

         var top5Query = 'select distinct user.name,user.email_id,user.university_id,user.phone,user.photo from user inner join user_qualification on user.id=user_qualification.user_id where user_qualification.qualification_id =2 order by user_qualification.percentage desc limit 5';
         var top5Result = yield databaseUtils.executeQuery(top5Query);
         var top5Details = top5Result[0];
 

        yield this.render('home',{

            top5Details: top5Details,
            top5List: top5Result,

        });
    },

    searchskill: function* (next) {
        var search=this.request.body.search;
         var val='/api/search?s='+ encodeURIComponent(search);
         
    this.redirect(val);
    },

    logout: function* (next)
    {
        var sessionId = this.cookies.get("SESSION_ID");
        if(sessionId)
            {
            sessionUtils.deleteSession(sessionId);
            }
        this.cookies.set("SESSION_ID", '', {expires: new Date(1), path: '/'});

        this.redirect('/');
    }
}
